public class Saving extends BankAccount
{
    int balance=0;
   
    
    @Override
    public void deposit(int amount)
    {
        if(amount<=50000)
        balance=balance+amount;
        else
        System.out.println("Limit exceeded");
    }


    @Override
    public void withdraw(int amount)
    {
        if((balance-amount)>10000)
        balance=balance-amount;
        else
        System.out.println("Less Balance");
    }

    @Override
    public int getBalance()
    {
        return balance;
    }
}