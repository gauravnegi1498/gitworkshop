public class TwoDArray
{
    static int c=10;
    public static void main(String args[])
    {
        int [][] arr = new int[4][4];
        
        populateArray(arr, c);
        displayArray(arr);
    }

    public static void populateArray(int[][] arr,int c)
    {
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                arr[i][j]=c;
                c++;
            }
        }
    }

    public static void displayArray(int[][] arr)
    {
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                System.out.print(arr[i][j]+" ");
            }
            System.out.println();
        }
    }
}