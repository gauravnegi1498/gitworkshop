public class Padietrician extends Doctor
{
    public Padietrician(String s)
    {
        super.names(s);
        System.out.println("In padietrician class name is: "+s);
    }
    public void treatPatient()
    {
        treatKids();
    }

    public void treatKids()
    {
        System.out.println("Treating Kids");
    }
    public String toString()
    {
        return "GN";
    }
}