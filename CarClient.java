public class CarClient
{
    public static void main(String args[])
    {
        int i=89;
        int[] array=new int[10];
        Car myCar=new Car();
        Car myNewCar=new Car();

        myCar.accelerate();
        myCar.accelerate();
        myCar.accelerate();
        myCar.accelerate();

        myNewCar.accelerate();
        myNewCar.accelerate();

        System.out.println("myCar speed is="+myCar.getCurrentSpeed());
        System.out.println("myCar speed is="+myNewCar.getCurrentSpeed());
    }
}
