public class GooglePay implements PaymentGateWay, RechargeMobile
{
    public void transact(String from, String to, double amount, String remark)
    {
        System.out.println("Initiating payments "+from+" to "+to+" of "+amount+" for "+remark);
        System.out.println("You got reward of 50 point");
    }

    public void recharge(String mobileno, double amount)
    {
        System.out.println("Initiating recharge of "+amount+" in "+mobileno);
        System.out.println("You got reward of 20 point");
    }


}