import java.util.*;
public class PaymentClient
{
    public static void main(String args[]) {
        Scanner sc=new Scanner(System.in);

        
        System.out.println("Enter option:");
        System.out.println("1- Google Pay \n 2-PhonePay");
        int option=sc.nextInt();
        PaymentGateWay gateway=null;
        switch(option)
        {
            case 1:
            gateway=new GooglePay();
            break;

            case 2:
            gateway=new PhonePay();
            break;

            default:
            gateway=new PhonePay();
            break;
        }
        gateway.transact("Gaurav","Atul",35,"Lunch");
        
        
    }
}
