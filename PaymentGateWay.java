public interface PaymentGateWay
{
    void transact(String from,String To, double amount, String remark);
}