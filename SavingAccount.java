public class SavingAccount
{
    private double balance;
    private String name;
    private static int counter=1000;
    private int accID;

    public SavingAccount(String name,double balance)
    {
        this.name=name;
        this.balance=balance;
        this.accID=counter++;
    }
    public SavingAccount(String name)
    {
        this.name=name;
        this.accID=counter++;

    }
    public void deposit(double amount)
    {
        this.balance=this.balance+amount;
    }
    public void deposit(double amount,String st)
    {
        this.balance=this.balance+amount;
        System.out.println(st);
    }
    public double withdraw(double amount)
    {
        if(this.balance>=amount)
        this.balance=this.balance-amount;
        else
        System.out.println("Less Balance");
        return this.balance;
    }
    public double display()
    {
        return this.balance;
    }
    public String getname()
    {
        return this.name;
    }
    public int getID()
    {
        return this.accID;
    }
}