public abstract class BankAccount
{
    int amount;
    public abstract void deposit(int amount);

    public abstract void withdraw(int amount);

    public abstract int getBalance();
}