public class Client
{
    public static void main(String args[])
    {
        BankAccount saving=new Saving();
        saving.deposit(4000);
        saving.deposit(3000);
        saving.deposit(20);
        saving.deposit(10000);
        saving.deposit(80);
        saving.deposit(40);
        System.out.println("You have: "+ saving.getBalance());
        saving.withdraw(2000);
        System.out.println("In saving account You have: "+ saving.getBalance());
        saving.withdraw(5000);
        saving.withdraw(5000);
        saving.withdraw(200);
        saving.withdraw(100);
        saving.withdraw(2000);
        saving.withdraw(2000);


        BankAccount current=new Current();
        current.deposit(10000);
        current.deposit(20000);
        current.deposit(400);
        System.out.println("In current account You have: "+ current.getBalance());
        current.withdraw(3000);

        BankAccount salary=new Salary();
        salary.deposit(3000);
        salary.deposit(2000);
        System.out.println("In salary account You have: "+ salary.getBalance());
        salary.withdraw(1000);
        salary.withdraw(5000);
        salary.withdraw(100);

    }
}