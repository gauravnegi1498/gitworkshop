package com.hexaware.exceptions;
import java.util.*;

public class ExceptionDemo {
public static void main(String args[])
{
Scanner sc=new Scanner(System.in);
System.out.println("Enter a no.");
int divident=sc.nextInt();

System.out.println("Enter a divisor");
int divisor=sc.nextInt();
float res=0;
try
{
res=(divident/divisor);
}
catch(ArithmeticException ex)
{
System.out.println("Please enter a valid value of divisor");
}
if(res>0)
	System.out.println("Result is:"+res);
sc.close();
}
}
