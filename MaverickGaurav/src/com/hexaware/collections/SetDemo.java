package com.hexaware.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {
	public static void main(String args[])
	{
		Set<BankAccount> s=new HashSet<>();
		s.add(new BankAccount(101,"g",250600));
		s.add(new BankAccount(102,"g",254000));
		s.add(new BankAccount(103,"g",257000));
		
		Iterator<BankAccount> it=s.iterator();
		while(it.hasNext())
		{
			System.out.println(it.next());
		}
		System.out.println(s.size());
	}

}
