package com.hexaware.collections;

public class BankAccount {
	int balance;
	String name;
	int ID;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		if (ID != other.ID)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	public BankAccount( int iD, String name,int balance) {
		super();		
		ID = iD;
		this.name = name;
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "BankAccount [balance=" + balance + ", name=" + name + ", ID=" + ID + "]";
	}

}
