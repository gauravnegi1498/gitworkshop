package com.hexaware.bankusinglist;
import java.util.ArrayList;
import java.util.List;

public class BankList
{
    List<SavingAccountNew> accountslist=new ArrayList<>();  
    private static int index=0;

    public List<SavingAccountNew> addBankAccount(SavingAccountNew accounts)
    {
        if(index<10)
        {
            accountslist.add(accounts);
            index++;
        }
        else
        System.out.println("NO Space");
        
        return accountslist;
    }
    public List<SavingAccountNew> deleteAccount(int in)
    {
        for(int i=0;i<index;i++)
        {
        	if(accountslist.get(i).getID()==in)
        		accountslist.remove(i);
        }
        return accountslist;
    }
    public void fetchDetail(List<SavingAccountNew> accountslist)
    {
        for(SavingAccountNew it:accountslist)
        {
            System.out.println(it.display());
        }
    }

}