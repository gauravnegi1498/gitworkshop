package com.hexaware.bankusinglist;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapDemo {
	public static void main(String args[]) {
		SavingAccountNew client1=new SavingAccountNew("gaurav",2000);
		SavingAccountNew client2=new SavingAccountNew("atul",3000);
		SavingAccountNew client3=new SavingAccountNew("shachit",5000);
		SavingAccountNew client4=new SavingAccountNew("shaurya",2500);
		
		Map<Integer, SavingAccountNew> map=new HashMap<>();
		map.put(1000,client1);
		map.put(1002,client2);
		map.put(1003,client3);
		map.put(1005,client4);
		
		SavingAccountNew sa=map.get(1000);
		System.out.println("Bank account object for 1000 is= "+sa);
		
//		Set<Integer> keys=map.keySet();
//		
//		Iterator<Integer> it=keys.iterator();
//		
//		while(it.hasNext())
//		{
//			System.out.println(map.get(it.next()));
//		}
		
//		Collection<SavingAccountNew> values=map.values();
//		Iterator<SavingAccountNew> it=values.iterator();
//		while(it.hasNext())
//		{
//			SavingAccountNew sa1=(SavingAccountNew) it.next();
//			System.out.println(client1);
//		}
		
		Set<Map.Entry<Integer, SavingAccountNew>> entrySet=map.entrySet();
		Iterator<Map.Entry<Integer, SavingAccountNew>> it=entrySet.iterator();
		
		while(it.hasNext())
		{
			Map.Entry<Integer, SavingAccountNew> entry = it.next();
			System.out.println("Key is"+entry.getKey()+" ,Value is"+entry.getValue());
		}
	}
}
