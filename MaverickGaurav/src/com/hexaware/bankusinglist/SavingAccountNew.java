package com.hexaware.bankusinglist;



public class SavingAccountNew implements Comparable<SavingAccountNew>
{
    private double balance;
    private String name;
    private static int counter=1000;
    int accID;

    public SavingAccountNew(String name,double balance)
    {
        this.name=name;
        this.balance=balance;
        this.accID=counter++;
    }
    public SavingAccountNew(String name)
    
    {
        this.name=name;
        this.accID=counter++;

    }
    public void deposit(double amount)
    {
        this.balance=this.balance+amount;
    }
    public void deposit(double amount,String st)
    {
        this.balance=this.balance+amount;
        System.out.println(st);
    }
    public double withdraw(double amount)
    {
        if(this.balance>=amount)
        this.balance=this.balance-amount;
        else
        System.out.println("Less Balance");
        return this.balance;
    }
    public double display()
    {
        return this.balance;
    }
    public double getBalance()
    {
        return this.balance;
    }
    public String getname()
    {
        return this.name;
    }
    public int getID()
    {
        return this.accID;
    }
    
    public int compareTo(SavingAccountNew savingaccount)
    {
    	int result=(int)this.accID-savingaccount.accID;
    	//int res=this.name.compareTo(SavingAccountNew.name);
    	return result;
    }
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accID;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SavingAccountNew other = (SavingAccountNew) obj;
		if (accID != other.accID)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "SavingAccountNew [balance=" + balance + ", name=" + name + ", accID=" + accID + "]";
	}
	
    
   
}
