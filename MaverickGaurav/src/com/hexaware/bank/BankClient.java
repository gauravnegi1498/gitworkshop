package com.hexaware.bank;

public class BankClient
{
    public static void main(String args[])
    {

        SavingAccount client1=new SavingAccount("gaurav",5000);
        SavingAccount client2=new SavingAccount("atul",1000);
        client1.deposit(200);
        client1.deposit(200,"First salary");
        System.out.println(client1.getname()+" with account ID "+client1.getID()+" has amount "+client1.display());
        client1.withdraw(700);
        client1.withdraw(800);
        client1.withdraw(1000);

        System.out.println(client1.getname()+" with account ID "+client1.getID()+" has amount "+client1.display());

        client2.deposit(6600);
        System.out.println(client2.getname()+" with account ID "+client2.getID()+" has amount "+client2.display());
        client2.withdraw(700);
        client2.withdraw(800);
        client2.withdraw(1000);

        System.out.println(client2.getname()+" with account ID "+client2.getID()+" has amount "+client2.display());
/*
        BankList list=new BankList();
        list.addBankAccount(client1);
        list.addBankAccount(client2);
*/
     BankList bl=new BankList();
     bl.addBankAccount(client1);
     bl.addBankAccount(client2);
     
     bl.fetchDetail();
     
     bl.deleteAccount(1001);
     bl.fetchDetail();

    }
}