package com.hexaware.bank;

public interface BankInterface {
	public void addBankAccount(SavingAccount accounts);
	public int deleteAccount(int index);
	public int fetchDetail(int id);
}
