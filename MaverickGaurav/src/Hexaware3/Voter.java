package Hexaware3;

import java.util.Scanner;

public class Voter {
	public static void main(String args[])
	{
		int x;
		Scanner sc=new Scanner(System.in);
		x=sc.nextInt();
		
		try
		{
			if(x<18)
			throw new InvalidAgeException("Voter is under age");
			else
			System.out.println("Valid Voter");
		}
		catch(InvalidAgeException ex)
		{
			System.out.println(ex.getMessage());
			//throw ex;
		}
		
	}
	
}
