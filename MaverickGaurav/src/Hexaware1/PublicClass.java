package Hexaware1;

public class PublicClass {
	
	public void publicMethod()
	{
		System.out.println("Public method");
	}

	protected void protectedMethod()
	{
		System.out.println("Protected method");
	}
	
	void defaultMethod()
	{
		System.out.println("Default method");
	}
	
	private void privateMethod()
	{
		System.out.println("Private method");
	}
}
