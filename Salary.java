public class Salary extends BankAccount
{
    int balance=0;
    
    
    @Override
    public void deposit(int amount)
    {
        balance=balance+amount;
    }


    @Override
    public void withdraw(int amount)
    {
        if((balance-amount)>=0)
        balance=balance-amount;
        else
        System.out.println("Less Balance");
    }

    @Override
    public int getBalance()
    {
        return balance;
    }
}