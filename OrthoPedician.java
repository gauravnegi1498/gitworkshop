public class OrthoPedician extends Doctor
{

    public OrthoPedician(String s)
    {
        super.names(s);
        System.out.println("In OrthoPedician class name is: "+s);
    }

    public void treatPatient()
    {
        conductCTscan();
        conductXRay();
    }


 public void conductCTscan()
 {
     System.out.println("Conducting CTscan");
 }   

 public void conductXRay()
 {
     System.out.println("Conducting X-Ray");
 } 
}